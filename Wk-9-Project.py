'''
Project: Shape Drawing Program
Written By: Winston Cadwell
Date: 11/9/16
Description: This program asks the user if what shape they want to make. It then asks them for several parameters:
line color, fill color, size, and pen shape. Finally it draws the shape.
'''
import turtle as t  # import turtle
import random as r  # import random


class Shapes():  # a class to store shape functions

    def __init__(self, fill_color, line_color, pen_shape, size):  # init variables to store shape properties
        self.fill_color = fill_color  # define fill_color
        self.line_color = line_color  # define line_color
        self.pen_shape = pen_shape  # define pen_shape
        self.size = size  # define size

    def square(self):  # method for making a square
        t.Screen()  # start a new turtle screen
        t.pencolor(self.line_color)  # set pen color to var line_color
        t.fillcolor(self.fill_color)  # set fillcolor to fill_color
        t.shape(self.pen_shape)  # set shape of pen to var pen_shape

        t.begin_fill()  # start filling shape

        for i in range(4):  # loop 4 times
            t.forward(self.size)  # move forward requested size
            t.right(90)  # turn 90 degrees

        t.end_fill()  # stop filling shape

    def triangle(self):  # method for making a triangle
        t.Screen()  # start a new turtle screen
        t.pencolor(self.line_color)  # set pen color to var line_color
        t.fillcolor(self.fill_color)  # set fillcolor to fill_color
        t.shape(self.pen_shape)  # set shape of pen to var pen_shape

        t.begin_fill()  # start filling shape

        for i in range(3):  # loop 3 times
            t.forward(self.size)  # move forward requested size
            t.right(120)  # turn right 120 degrees

        t.end_fill()  # stop filling shape

    def star(self): # method for making a star
        t.Screen()  # start a new turtle screen
        t.pencolor(self.line_color)  # set pen color to var line_color
        t.fillcolor(self.fill_color)  # set fillcolor to fill_color
        t.shape(self.pen_shape)  # set shape of pen to var pen_shape

        t.begin_fill()  # start filling shape

        for i in range(5):  # loop 5 times
            t.forward(self.size)  # move forward requested size
            t.right(180-36)  # turn right 144 degrees

        t.end_fill()  # stop filling shape

    def hexagon(self):  # method for making a hexagon
        t.Screen()  # start a new turtle screen
        t.pencolor(self.line_color)  # set pen color to var line_color
        t.fillcolor(self.fill_color)  # set fillcolor to fill_color
        t.shape(self.pen_shape)  # set shape of pen to var pen_shape

        t.begin_fill()  # start filling shape

        for i in range(6):  # loop 6 times
            t.forward(self.size)  # move forward requested size
            t.right(60) # turn right 60 degrees

        t.end_fill()  # stop filling shape


print("Welcome to ShapeGen v2.11.34")  # say welcome to the program
# Ask what shape they want and tell them their options
shape = input(
    "What shape do you want to make?\n Your options are:\n  square\n  triangle\n  star\n  hexagon\n>>> ").lower()
# Ask what fill color they want and give the user a list of options
fill_col = input(
    "What fill color do you want?\n Your options are:\n red\n blue\n green\n black\n pink\n purple\n brown\n>>> ").lower()
# Ask what fill color they want and give the user a list of options
line_col = input(
    "What line color do you want?\n Your options are:\n red\n blue\n green\n black\n pink\n purple\n brown\n>>> ").lower()
# Ask how big the user wants their shape
size = input("How big do you want your shape? You can enter any whole number, under 500 fits best on the screen.\n>>> ")
# Ask what pen shape the user wants
pen = input("What pen shape do you want? Your options are:\n square\n triangle\n circle\n turtle\n>>> ").lower()

shape1 = Shapes(fill_col,line_col, pen, int(size))  # use the Shapes class to define a shape with the users parameters

if (shape == "square"):  # if user entered square
    shape1.square()  # call square method

elif (shape == "triangle"):  # if user entered triangle
    shape1.triangle() # call triangle method

elif (shape == "hexagon"):  # if user entered hexagon
    shape1.hexagon() # call hexagon method

elif (shape == "star"):  # if user entered star
    shape1.star()  # call star mehtod

else:  # if the user entered something else
    print("The shape you chose is not an option!")  # tell them they entered an invalid shape
    exit(5)

t.done()  # tell turtle we are done drawing
